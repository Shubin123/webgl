import { Client } from "@gradio/client";
import * as THREE from "three";
import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import { GLTFLoader } from "three/addons/loaders/GLTFLoader.js";

const inputText = document.getElementById("phrase");
const generate = document.getElementById("run");
const prompt_holder = document.getElementById("prompt-holder");
const widthSlider = document.getElementById("widthSlider");

const heightSlider = document.getElementById("heightSlider");
const seedSlider = document.getElementById("seedSlider");
const stepsSlider = document.getElementById("stepsSlider");
const styleOptions = document.getElementById("styleOptions");

const imageGallery = document.getElementById("imageGallery");
const outputGallery = document.getElementById("outputGallery");

const videoPlayer = document.getElementById("videoPlayer");
const videoPlayerSrc = document.getElementById("videoPlayerSrc");

const width = window.innerWidth,
  height = window.innerHeight;

let renderer, scene, camera, directionalLight, model, loader;
let offset = 0;
let firstClick = false;
let models = [];

function init(glbURL) {
  // init
  camera = new THREE.PerspectiveCamera(70, width / height, 0.01, 10);
  camera.position.z = 1;

  scene = new THREE.Scene();
  scene.background = new THREE.Color(0x1e1e2f); // Set background color to sky blue

  // const ambientLight = new THREE.AmbientLight(0xffffff, 2); // soft white light
  // ambientLight.intensity = 100;
  // scene.add(ambientLight);

  directionalLight = new THREE.DirectionalLight(0xffffff, 1);
  directionalLight.intensity = 10;
  directionalLight.position.set(1, 1, 1); // position the light

  scene.add(directionalLight);

  // const hemiLight = new THREE.HemisphereLight(0xffc0cb, 0x444444);
  // hemiLight.intensity = 20; // Increasing the intensity, default is 1
  // hemiLight.position.set(1, 1, 1);
  // scene.add(hemiLight);

  loader = new GLTFLoader();
  loader.load(
    glbURL,
    async function (gltf) {
      models.push(gltf.scene);
      // console.log(model);
      gltf.scene.scale.set(1, 1, 1); // Adjust the scale if needed
      // model.position.y = 0;
      // await renderer.compileAsync( model, camera, scene );
      scene.add(gltf.scene);

      // render();
    },

    undefined,

    function (error) {
      console.error("An error happened:", error);
    }
  );

  renderer = new THREE.WebGLRenderer({ antialias: false });
  renderer.setPixelRatio(window.devicePixelRatio);
  
  renderer.setSize(
    outputGallery.getBoundingClientRect().width,
    outputGallery.getBoundingClientRect().width * 0.5625
  );
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  renderer.toneMappingExposure = 1;

  const container = document.createElement("div");
  centerDiv(container);
  outputGallery.appendChild(container);
  container.appendChild(renderer.domElement);

  const controls = new OrbitControls(camera, renderer.domElement);
  // controls.addEventListener("change", render); // use if there is no animation loop
  controls.enableZoom = true;
  controls.minDistance = 2;
  controls.maxDistance = 10;
  controls.target.set(0, 0, -0.2);
  controls.update();
  renderer.setAnimationLoop(animation);
  // window.addEventListener( 'resize', onWindowResize );
}

function addModel(glbUrl) {
  loader.load(
    glbUrl,
    async function (gltf) {
      models.push(gltf.scene);
      console.log(gltf.scene);
      offset += 1;
      gltf.scene.position.x = offset;
      gltf.scene.scale.set(1, 1, 1); // Adjust the scale if needed
      // model.position.y = 0;
      // await renderer.compileAsync( model, camera, scene );
      scene.add(gltf.scene);

      // render();
    },

    undefined,

    function (error) {
      console.error("An error happened:", error);
    }
  );
}

let upload = true;
const userImageInput = document.getElementById("userImageInput");

generate.addEventListener("click", async () => {
  try {
    // if (!upload) {
   
    
      addModel(
        await testAPISequence(
          await processImageToBlob(await textToImageRequest())
        )
      );
    

    // } else {
    //   if (userImageInput.files.length == 1){
    //     // console.log("file exists");

    //     init(await testAPISequence(await processImageToBlob(URL.createObjectURL(userImageInput.files[0]))));

    //   }
    // }
  } catch (error) {
    console.log("Error:", error);
  }
});

async function textToImageRequest() {
  console.log(styleOptions.value);
  console.log(inputText.value);
  console.log(widthSlider.value);
  console.log(heightSlider.value);
  console.log(seedSlider.value);
  console.log(stepsSlider.value);

  const app = await Client.connect("KingNish/Instant-Image");
  const result = await app.predict("/run", [
    inputText.value, // string  in 'Prompt' Textbox component
    "Hello!!", // string  in 'Negative prompt' Textbox component
    styleOptions.value, // string  in 'Image Style' Radio component
    true, // boolean  in 'Use negative prompt' Checkbox component
    Number(seedSlider.value), // number (numeric value between 0 and 2147483647) in 'Seed' Slider component
    Number(widthSlider.value), // number (numeric value between 256 and 4192) in 'Width' Slider component
    Number(heightSlider.value), // number (numeric value between 256 and 4192) in 'Height' Slider component
    Number(stepsSlider.value), // number (numeric value between 4 and 20) in 'Steps' Slider component
    true, // boolean  in 'Randomize seed' Checkbox component
  ]);
  // console.log(result);
  // console.log(result.data)
  console.log(result.data[0].url);

  var src = result.data[0].url;
  var img = document.createElement("img");
  img.src = src;
  imageGallery.appendChild(img);
  return src;
}

function animation(time) {
  // requestAnimationFrame(render);
  // directionalLight.intensity += Math.sin(t);
  // console.log(models);
  models.forEach((model) => {
    model.rotateOnAxis(new THREE.Vector3(0, 1, 0), 0.005);
  });
  // console.log(t);
  // console.log(renderer)
  // crate.rotation.x = time / 2000;
  // crate.rotation.y = time / 1000;

  renderer.render(scene, camera);
}

// import { uploadFiles, listModels } from "https://esm.sh/@huggingface/hub"
// // models = list_models()
// console.log(listModels(endpoint="https://fighting-jungle-remote-volunteers.trycloudflare.com/"))

// const client = await Client.connect("https://contains-consolidated-you-features.trycloudflare.com/");
// const result = await client.predict("/start_session", {
// });

const imageUrl = "./image.png";

const serverURL = "JeffreyXiang/TRELLIS";
// const serverURL = "https://tions-include-designed-reduce.trycloudflare.com/";
const imagePreprocessRequest = ["/preprocess_image", "/preprocess_image_1"];
const imagePreprocessRequestIndex = 0;

let result;

async function processImageToBlob(imageUrl) {
  // Fetch the image from the provided URL
  const response = await fetch(imageUrl);
  if (!response.ok) {
    throw new Error(`Failed to fetch image: ${response.statusText}`);
  }
  const imageBlob = await response.blob();

  return imageBlob;
}

async function testAPISequence(imageBlob) {
  let resultr;
  try {
    const client = await Client.connect(serverURL);

    // Call the start_session endpoint (if available)
    await client.predict("/start_session", {});

    // Call preprocess_image_1
    const preprocessResult = await client.predict(
      imagePreprocessRequest[imagePreprocessRequestIndex],
      {
        image: imageBlob,
      }
    );

    console.log("Preprocess Result:", preprocessResult.data[0].url);

    // Call image_to_3d
    const convertResult = await client.predict("/image_to_3d", {
      image: preprocessResult.data[0],
      multiimages: [],
      seed: 0,
      ss_guidance_strength: 7.5,
      ss_sampling_steps: 12,
      slat_guidance_strength: 3,
      slat_sampling_steps: 12,
      multiimage_algo: "stochastic",
    });

    // console.log("3D Conversion Result:", convertResult);
    // console.log(convertResult.data[0].video.url);
    // console.log(videoPlayer);
    videoPlayer.src = convertResult.data[0].video.url;
    
    // videoPlayer.load(); // may not work untestd
    // videoPlayer.play(); //same
    
    
    resultr = await client.predict("/extract_glb", {
      mesh_simplify: 0.95,
      texture_size: 1024,
    });

    console.log(resultr);
    console.log(resultr.data[0].url);
  } catch (error) {
    console.error("Error testing API sequence:", error);
  }

  return resultr.data[0].url;
}

async function testAllLambdaEndpoints() {
  try {
    const client = await Client.connect(serverURL);

    // List of lambda endpoints to test
    const lambdaEndpoints = [
      "/lambda",
      "/lambda_1",
      "/lambda_2",
      "/lambda_3",
      "/lambda_4",
      "/lambda_5",
      "/lambda_6",
    ];

    for (const endpoint of lambdaEndpoints) {
      console.log(`Testing endpoint: ${endpoint}`);

      const result = await client.predict(endpoint, {
        // No parameters for these endpoints
      });

      console.log(`Result from ${endpoint}:`, result.data);
    }
  } catch (error) {
    console.error("An error occurred:", error);
  }
}





function centerDiv(div) {
    div.style.position = "sticky";
    div.style.left = "20%";
    // div.style.width = "100%"
    //    div.style.right = "%";
  }
  





  window.addEventListener("resize", () => {
    if (!renderer) {
      return;
    }
  
  
    renderer.setSize(
      outputGallery.getBoundingClientRect().width,
      outputGallery.getBoundingClientRect().width * 0.5625
    );
  });
  


init("./burger.glb");

  
